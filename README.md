# CifsWatchDog

SystemD service used to ensure an SMB share has not stopped responding. This script was last modified in February 2018 and is no longer maintained.
